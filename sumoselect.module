<?php

/**
 * @file
 * SumoSelect hook implementations.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_page_attachments().
 */
function sumoselect_page_attachments(array &$attachments) {
  $config = \Drupal::config('sumoselect.settings');

  $isAdminRoute = \Drupal::service('router.admin_context')->isAdminRoute();
  if ($isAdminRoute) {
    $enabled = $config->get('enable_admin');
    $pathPatterns = $config->get('admin_path_patterns');
    $excludedPatterns = $config->get('admin_path_excluded_patterns');
  }
  else {
    $enabled = $config->get('enable_regular');
    $pathPatterns = $config->get('regular_path_patterns');
    $excludedPatterns = $config->get('regular_path_excluded_patterns');
  }

  /** @var \Drupal\sumoselect\RequestPathChecker $requestPathChecker */
  $requestPathChecker = \Drupal::service('sumoselect.request_path_checker');
  /** @var \Drupal\Core\Render\BubbleableMetadata $bubbleableMetaData */
  $checkPatterns = $requestPathChecker->checkPatterns($pathPatterns, $bubbleableMetaData);
  $checkExcludedPatterns = $requestPathChecker->checkExcludedPatterns($excludedPatterns, $bubbleableMetaData);

  if (!($enabled && $checkPatterns && $checkExcludedPatterns)) {
    return;
  }
  BubbleableMetadata::createFromRenderArray($attachments)
    ->merge($bubbleableMetaData)
    ->applyTo($attachments);

  $attachments['#attached']['library'][] = 'sumoselect/sumoselect.drupal';
  $attachments['#attached']['library'][] = 'sumoselect_lib/sumoselect.css';
  $attachments['#attached']['library'][] = 'sumoselect/sumoselect.drupal.css';

  $options['locale'] = [
    $config->get('locale_ok'),
    $config->get('locale_cancel'),
    $config->get('locale_selectall'),
  ];
  $options['searchText'] = $config->get('search_text');
  $options['noMatch'] = $config->get('no_match');
  $options['placeholder'] = $config->get('placeholder');
  $options['csvDispCount'] = (int) $config->get('csv_disp_count');
  $options['captionFormat'] = $config->get('caption_format');
  $options['captionFormatAllSelected'] = $config->get('caption_format_all_selected');
  $options['floatWidth'] = (int) $config->get('float_width');
  $options['forceCustomRendering'] = (bool) $config->get('force_custom_rendering');
  $options['okCancelInMulti'] = (bool) $config->get('ok_cancel_in_multi');
  $options['isClickAwayOk'] = (bool) $config->get('is_click_away_ok');
  $options['selectAll'] = (bool) $config->get('select_all');
  $options['search'] = (bool) $config->get('search');
  $options['nativeOnDevice'] = $config->get('native_on_device');

  $settings['options'] = $options;
  $settings['selector'] = $config->get('selector');
  $settings['selectorToExclude'] = $config->get('selector_to_exclude');
  $settings['selectorToInclude'] = $config->get('selector_to_include');

  $attachments['#attached']['drupalSettings']['sumoselect'] = $settings;
}
